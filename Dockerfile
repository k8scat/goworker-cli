FROM golang:1.16-alpine as builder
WORKDIR /goworker-cli
COPY . .
RUN apk --no-cache add make git && \
    make mod build

FROM alpine:latest
LABEL maintainer="K8sCat <k8scat@gmail.com>"
WORKDIR /goworker-cli
COPY --from=builder /goworker-cli/bin/goworker-cli .
COPY --from=builder /goworker-cli/configs/. ./configs
EXPOSE 3389
CMD ["./goworker-cli", "-c", "configs/config.json"]
