
NAME = goworker-cli
BUILD_FLAGS = CGO_ENABLED=0 GOOS=linux GOARCH=amd64

IMAGE_VERSION = latest
IMAGE = $(NAME):$(IMAGE_VERSION)

GO = go

build:
	$(BUILD_FLAGS) $(GO) build -trimpath -o bin/$(NAME) main.go

dev:
	$(GO) run main.go -c configs/config.json

start-compose:
	docker-compose up -d

build-image:
	docker build -t $(IMAGE) .

remove-image:
	docker rmi $(IMAGE)

stop-compose:
	docker-compose down

mod:
	$(GO) mod tidy
