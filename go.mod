module github.com/k8scat/goworker-cli

go 1.16

require (
	github.com/getsentry/sentry-go v0.10.0
	github.com/gin-gonic/gin v1.7.1
	github.com/tidwall/gjson v1.8.0
)
