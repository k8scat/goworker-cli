package config

var (
	GlobalConfig *Config
)

type Config struct {
	Port int `json:"port"`

	OnesTeamUUID  string `json:"ones_team_uuid"`
	OnesUserUUID  string `json:"ones_user_uuid"`
	OnesAuthToken string `json:"ones_auth_token"`

	FeishuEncryptKey        string `json:"feishu_encrypt_key"`
	FeishuVerificationToken string `json:"feishu_verification_token"`
	FeishuAppID             string `json:"feishu_app_id"`
	FeishuAppSecret         string `json:"feishu_app_secret"`
	FeishuUserID            string `json:"feishu_user_id"`

	WeeklyReportTemplateFile string `json:"weekly_report_template_file"`
	WeeklyReportSpaceUUID    string `json:"weekly_report_space_uuid"`
	WeeklyReportOkrURL       string `json:"weekly_report_okr_url"`

	GithubAccessToken              string `json:"github_access_token"`
	GithubPullRequestsTemplateFile string `json:"github_pull_requests_template_file"`
}
