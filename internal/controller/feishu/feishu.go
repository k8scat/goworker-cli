package feishu

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"text/template"

	"github.com/gin-gonic/gin"
	"github.com/k8scat/goworker-cli/internal/config"
	"github.com/k8scat/goworker-cli/internal/service/feishu"
	"github.com/k8scat/goworker-cli/internal/service/ones"
	onessdk "github.com/k8scat/goworker-cli/pkg/api/ones"
)

type WeeklyReport struct {
	DoneTasks   []*onessdk.Task
	UndoneTasks []*onessdk.Task
}

func CallbackHandler(c *gin.Context) {
	b, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	callback := new(feishu.Callback)
	if err := json.Unmarshal(b, callback); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	if callback.Encrypt != "" {
		callback, err = feishu.ParseCallback(callback.Encrypt)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
	log.Printf("callback: %+v", callback)
	if callback.Token != config.GlobalConfig.FeishuVerificationToken {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "unverified",
		})
		return
	}

	switch callback.Type {
	case feishu.CallbackTypeUrlVerification:
		c.JSON(http.StatusOK, gin.H{
			"challenge": callback.Challenge,
		})
		return
	case feishu.CallbackTypeEventCallback:
		event := callback.Event
		switch event.Type {
		case feishu.EventTypeMessage:
			msg := strings.Split(strings.Trim(event.Text, " "), " ")
			command := msg[0]
			var resultMsg string
			switch command {
			case "help":
				resultMsg = `指令列表:
1. 创建周报, 参数: weekNumber, sprint_uuid, parent_page_uuid
2. 生成合并请求列表

使用方式: command_index arg1 arg2`
			case "1":
				weekNumber, err := strconv.Atoi(msg[1])
				if err != nil {
					resultMsg = fmt.Sprintf("parse week number failed: %v", err)
				}
				sprintUUID := msg[2]
				parentPageUUID := msg[3]
				reportURL, err := GenerateWeeklyReport(weekNumber, sprintUUID, parentPageUUID)
				if err != nil {
					resultMsg = fmt.Sprintf("generate weekly report failed: %v", err)
				} else {
					resultMsg = fmt.Sprintf("report url: %s", reportURL)
				}
			default:
				resultMsg = fmt.Sprintf("wrong command: %s\n\n发送 help 可以查看指令列表", event.Text)
			}
			feishu.SendMessage(resultMsg)
		}
	}
}

func GenerateWeeklyReport(weekNumber int, sprintUUID, parentPageUUID string) (reportURL string, err error) {
	var sprintTasks []*onessdk.Task
	if sprintTasks, err = ones.ListTasksBySprintAndAssign(sprintUUID, config.GlobalConfig.OnesUserUUID); err != nil {
		return
	}
	doneTasks := make(map[string][]string)
	undoneTasks := make([]string, 0)
	otherTasks := make([]string, 0)
	for _, task := range sprintTasks {
		taskType, taskName := parseTaskTitle(task.Name)
		if task.Status.UUID == onessdk.StatusUUIDDone {
			if taskType == "" {
				otherTasks = append(otherTasks, taskName)
			} else {
				doneTasks[taskType] = append(doneTasks[taskType], taskName)
			}
		} else {
			undoneTasks = append(undoneTasks, taskName)
		}
	}
	weeklyReport := map[string]interface{}{
		"doneTasks":   doneTasks,
		"undoneTasks": undoneTasks,
		"otherTasks":  otherTasks,
		"okrURL":      config.GlobalConfig.WeeklyReportOkrURL,
	}
	log.Printf("%+v", weeklyReport)
	var t *template.Template
	if t, err = template.ParseFiles(config.GlobalConfig.WeeklyReportTemplateFile); err != nil {
		return
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, weeklyReport); err != nil {
		return
	}
	title := fmt.Sprintf("[%d] 万华松", weekNumber)
	var reportPageUUID string
	if reportPageUUID, err = ones.Client.CreatePage(config.GlobalConfig.WeeklyReportSpaceUUID, parentPageUUID, title, buf.String()); err != nil {
		return
	}
	reportURL = onessdk.GenerateWikiURL(config.GlobalConfig.WeeklyReportSpaceUUID, reportPageUUID)
	return
}

func parseTaskTitle(title string) (string, string) {
	r := regexp.MustCompile(`\[(.+)\] (.+)`)
	if !r.MatchString(title) {
		return "", title
	} else {
		result := r.FindAllStringSubmatch(title, -1)
		return result[0][1], result[0][2]
	}
}
