package middleware

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func RequestLogger(c *gin.Context) {
	var b []byte
	var err error
	if c.Request.Body != nil {
		b, err = ioutil.ReadAll(c.Request.Body)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			return
		}
		log.Printf("request body: %s", string(b))
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "request body cannot be nil",
		})
		return
	}
	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(b))
	c.Next()
}
