package router

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/k8scat/goworker-cli/internal/controller/feishu"
	"github.com/k8scat/goworker-cli/internal/middleware"
)

func Run(port int) {
	r := gin.Default()
	r.Use(middleware.RequestLogger)

	r.POST("/feishu/callback", feishu.CallbackHandler)

	if err := r.Run(fmt.Sprintf(":%d", port)); err != nil {
		log.Fatal(err)
	}
}
