package feishu

import (
	"encoding/json"
	"errors"

	"github.com/k8scat/goworker-cli/internal/config"
	"github.com/k8scat/goworker-cli/pkg/util"
)

func ParseCallback(encrypt string) (*Callback, error) {
	if config.GlobalConfig.FeishuEncryptKey == "" {
		return nil, errors.New("feishu encrypt key cannot be empty")
	}

	b, err := util.AESDecrypt(encrypt, []byte(config.GlobalConfig.FeishuEncryptKey))
	if err != nil {
		return nil, err
	}
	callback := new(Callback)
	if err := json.Unmarshal(b, callback); err != nil {
		return nil, err
	}
	return callback, nil
}
