package feishu

import (
	"github.com/k8scat/goworker-cli/internal/config"
	"github.com/k8scat/goworker-cli/pkg/api/feishu"
)

var Client *feishu.Client

func InitClient() error {
	var err error
	Client, err = feishu.NewClient(config.GlobalConfig.FeishuAppID, config.GlobalConfig.FeishuAppSecret)
	return err
}
