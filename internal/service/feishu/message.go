package feishu

import (
	"fmt"

	"github.com/k8scat/goworker-cli/internal/config"
)

func SendMessage(format string, v ...interface{}) error {
	content := fmt.Sprintf(format, v...)
	return Client.SendTextMessage(content, "", "", config.GlobalConfig.FeishuUserID, "", "")
}
