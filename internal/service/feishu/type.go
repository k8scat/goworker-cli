package feishu

const (
	CallbackTypeUrlVerification = "url_verification"
	CallbackTypeEventCallback   = "event_callback"

	EventTypeMessage = "message"
)

type Callback struct {
	UUID      string `json:"uuid"`
	Challenge string `json:"challenge"`
	Token     string `json:"token"`
	Type      string `json:"type"`
	Encrypt   string `json:"encrypt"`
	Event     Event  `json:"event"`
	TS        string `json:"ts"`
}

type Event struct {
	AppID            string `json:"app_id"`
	TenantKey        string `json:"tenant_key"` // 企业标识
	Type             string `json:"type"`
	OpenID           string `json:"openid"`
	OpenMessageID    string `json:"open_message_id"`
	MsgType          string `json:"msg_type"`
	ChatType         string `json:"chat_type"`
	OpenChatID       string `json:"open_chat_id"`
	Text             string `json:"text"`
	TextWithoutAtBot string `json:"text_without_at_bot"`
	IsMention        bool   `json:"is_mention"`
	RootID           string `json:"root_id"`
	ParentID         string `json:"parent_id"`
}
