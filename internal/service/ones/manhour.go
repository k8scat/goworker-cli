package ones

import "github.com/k8scat/goworker-cli/pkg/api/ones"

func ListManhoursWeekly(sprintUUID, ownerUUID string) (manhours []*ones.Manhour, err error) {
	sprint, err := GetSprintByUUID(Client, sprintUUID)
	if err != nil {
		return nil, err
	}
	return Client.ListManhoursBySprintAndOwner(sprint.UUID, ownerUUID)
}
