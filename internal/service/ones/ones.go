package ones

import (
	"github.com/k8scat/goworker-cli/internal/config"
	"github.com/k8scat/goworker-cli/pkg/api/ones"
)

var Client *ones.Client

func InitClient() error {
	var err error
	Client, err = ones.NewClient(config.GlobalConfig.OnesTeamUUID, config.GlobalConfig.OnesUserUUID, config.GlobalConfig.OnesAuthToken)
	return err
}
