package ones

import (
	"github.com/k8scat/goworker-cli/internal/config"
	"github.com/k8scat/goworker-cli/pkg/api/ones"
)

func GetSprintByUUID(client *ones.Client, uuid string) (*ones.SprintPayload, error) {
	sprints, err := client.ListSprintsByTeam(config.GlobalConfig.OnesTeamUUID)
	if err != nil {
		return nil, err
	}
	for _, sprint := range sprints {
		if sprint.UUID == uuid {
			return sprint, nil
		}
	}
	return nil, nil
}
