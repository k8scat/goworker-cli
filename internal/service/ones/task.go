package ones

import (
	"encoding/json"
	"fmt"

	"github.com/k8scat/goworker-cli/pkg/api/ones"
	"github.com/tidwall/gjson"
)

func ListTasksBySprintAndAssign(sprintUUID, assignUUID string) (tasks []*ones.Task, err error) {
	query := `
	{
		tasks(
			filter: {
				sprint_equal: "%s"
				assign_equal: "%s"
			}
		) {
			name
			status {
				uuid
			}
		}
	}`
	query = fmt.Sprintf(query, sprintUUID, assignUUID)
	var content string
	if content, err = Client.GraphQL(query); err != nil {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.tasks").String()), &tasks)
	return
}
