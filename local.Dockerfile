FROM golang:1.16-alpine as builder
WORKDIR /goworker
COPY . .
ENV GOPROXY https://goproxy.io,direct
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories && \
    apk --no-cache add make git && \
    make mod build

FROM alpine:latest
LABEL maintainer="K8sCat <k8scat@gmail.com>"
WORKDIR /goworker
COPY --from=builder /goworker/bin/goworker .
COPY --from=builder /goworker/configs/. ./configs
EXPOSE 3389
CMD ["./goworker", "-c", "configs/config.json"]
