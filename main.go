package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"

	"github.com/k8scat/goworker-cli/internal/service/feishu"
	"github.com/k8scat/goworker-cli/internal/service/ones"

	"github.com/getsentry/sentry-go"
	"github.com/k8scat/goworker-cli/internal/config"
	"github.com/k8scat/goworker-cli/internal/router"
)

var (
	cfgFile string
	port    int
)

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)

	initFlags()
	loadConfig()
	initSentry()

	if err := feishu.InitClient(); err != nil {
		log.Fatalf("init feishu client failed: %v", err)
	}
	if err := ones.InitClient(); err != nil {
		log.Fatalf("init ones client failed: %v", err)
	}
	router.Run(port)
}

func initFlags() {
	flag.StringVar(&cfgFile, "c", "", "config file")
	flag.IntVar(&port, "p", 3389, "listen port")
	flag.Parse()
}

func initSentry() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://77a5c17b0e2c4b08918296971b01e9fa@o371841.ingest.sentry.io/5537224",
		BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			// 输出到日志文件
			log.Printf("event: %+v", event)
			return event
		},
	})
	if err != nil {
		log.Fatalf("sentry init failed: %s", err)
	}
}

func loadConfig() {
	b, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		log.Fatalf("read config failed: %v", err)
	}
	if err := json.Unmarshal(b, &config.GlobalConfig); err != nil {
		log.Fatalf("unmarshal config failed: %v", err)
	}
}
