package feishu

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/tidwall/gjson"
)

const (
	BaseAPI = "https://open.feishu.cn/open-apis"

	defaultAppID     = "cli_9f31f08205fc500b"
	defaultAppSecret = "S2Y0wE7QYpESPmn9qxAWjcqK6uPuMQuC"
)

type Client struct {
	AppID                string
	AppSecret            string
	TenantAccessToken    string
	TokenExpire          int64
	LastRefreshTokenTime int64
}

func NewClient(appID, appSecret string) (client *Client, err error) {
	if appID == "" || appSecret == "" {
		err = errors.New("appID and appSecret cannot be empty")
		return
	}
	client = &Client{
		AppID:     appID,
		AppSecret: appSecret,
	}
	return
}

func (c *Client) refreshTenantAccessToken() error {
	currentTime := time.Now().Unix()
	if currentTime-c.LastRefreshTokenTime < c.TokenExpire {
		return nil
	}
	api := fmt.Sprintf("%s/auth/v3/tenant_access_token/internal", BaseAPI)
	data := map[string]string{
		"app_id":     c.AppID,
		"app_secret": c.AppSecret,
	}
	b, _ := json.Marshal(data)
	body := bytes.NewReader(b)
	req, err := http.NewRequest(http.MethodPost, api, body)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Err resp: %v")
	}

	defer resp.Body.Close()
	b, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	raw := string(b)
	if gjson.Get(raw, "code").Int() != 0 {
		return fmt.Errorf("Refresh token failed: %v, resp: %v", raw, resp)
	}
	c.TenantAccessToken = gjson.Get(raw, "tenant_access_token").String()
	c.TokenExpire = gjson.Get(raw, "expire").Int()
	c.LastRefreshTokenTime = currentTime
	return nil
}

func (c *Client) Post(endpoint string, data map[string]interface{}) (*http.Response, error) {
	if err := c.refreshTenantAccessToken(); err != nil {
		return nil, err
	}
	b, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	body := bytes.NewReader(b)
	api := fmt.Sprintf("%s%s", BaseAPI, endpoint)
	req, err := http.NewRequest(http.MethodPost, api, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.TenantAccessToken))
	return http.DefaultClient.Do(req)
}

func (c *Client) Get(endpoint string, params *url.Values) (*http.Response, error) {
	if err := c.refreshTenantAccessToken(); err != nil {
		return nil, err
	}
	api := fmt.Sprintf("%s%s", BaseAPI, endpoint)
	req, err := http.NewRequest(http.MethodGet, api, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.TenantAccessToken))
	return http.DefaultClient.Do(req)
}
