package feishu

import (
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/tidwall/gjson"
)

// https://open.feishu.cn/document/ukTMukTMukTM/uUjNz4SN2MjL1YzM
func (c *Client) SendTextMessage(content, rootID, openID, userID, email, chatID string) error {
	if openID == "" && userID == "" && email == "" && chatID == "" {
		return errors.New("openID, userID, email and chatID cannot all be empty")
	}
	if content == "" {
		return errors.New("content cannot be empty")
	}

	endpoint := "/message/v4/send"
	data := map[string]interface{}{
		"msg_type": "text",
		"content": map[string]string{
			"text": content,
		},
		"open_id": openID,
		"user_id": userID,
		"email":   email,
		"chat_id": chatID,
	}
	resp, err := c.Post(endpoint, data)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	raw := string(b)
	if gjson.Get(raw, "code").Int() != 0 {
		return fmt.Errorf("Send text message failed: %s, resp: %v", raw, resp)
	}
	return nil
}
