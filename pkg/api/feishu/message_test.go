package feishu

import "testing"

func TestSendTextMessage(t *testing.T) {
	client, err := NewClient(defaultAppID, defaultAppSecret)
	if err != nil {
		t.Error(err)
	} else {
		if err := client.SendTextMessage("hello", "", "", "819d28c8", "", ""); err != nil {
			t.Error(err)
		}
	}
}
