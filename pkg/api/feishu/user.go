package feishu

import (
	"fmt"
	"io/ioutil"
	"net/url"

	"github.com/tidwall/gjson"
)

// https://open.feishu.cn/document/ukTMukTMukTM/uUzMyUjL1MjM14SNzITN
func (c *Client) GetUserIDMapByPhonesOrEmails(mobiles, emails []string) (map[string]string, error) {
	endpoint := "/user/v1/batch_get_id"
	params := &url.Values{
		"emails":  emails,
		"mobiles": mobiles,
	}
	resp, err := c.Get(endpoint, params)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	raw := string(b)
	if gjson.Get(raw, "code").Int() != 0 {
		return nil, fmt.Errorf("Get user id map failed: %s, resp: %v", raw, resp)
	}
	userIDMap := make(map[string]string)
	for _, mobile := range mobiles {
		userID := gjson.Get(raw, fmt.Sprintf("data.mobile_users.%s.0.user_id", mobile)).String()
		if userID != "" {
			userIDMap[mobile] = userID
		}

	}
	for _, email := range emails {
		data := gjson.Get(raw, "data.email_users").Map()
		if _, exists := data[email]; exists {
			userID := data[email].Array()[0].Map()["user_id"].String()
			if userID != "" {
				userIDMap[email] = userID
			}
		}
	}
	return userIDMap, nil
}
