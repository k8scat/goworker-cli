package feishu

import (
	"fmt"
	"testing"
)

func TestGetUserIDMapByPhonesOrEmails(t *testing.T) {
	client, err := NewClient(defaultAppID, defaultAppSecret)
	if err != nil {
		t.Error(err)
	} else {
		userIDMap, err := client.GetUserIDMapByPhonesOrEmails([]string{"16675410724"}, []string{"hsowan.me@gmail.com"})
		if err != nil {
			t.Error(err)
		}
		fmt.Printf("userIDMap: %+v", userIDMap)
	}
}
