package ones

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type LoginUser struct {
	UUID         string `json:"uuid"`
	Email        string `json:"email"`
	Name         string `json:"name"`
	NamePinyin   string `json:"name_pinyin"`
	Title        string `json:"title"`
	Avatar       string `json:"avatar"`
	Phone        string `json:"phone"`
	CreateTime   int64  `json:"create_time"`
	Status       int    `json:"status"`
	Channel      string `json:"channel"`
	Token        string `json:"token"`
	LicenseTypes []int  `json:"license_types"`
}

type LoginTeam struct {
	UUID          string   `json:"uuid"`
	Status        int      `json:"status"`
	Name          string   `json:"name"`
	Owner         string   `json:"owner"`
	Logo          string   `json:"logo"`
	CoverURL      string   `json:"cover_url"`
	Domain        string   `json:"domain"`
	CreateTime    int64    `json:"create_time"` // time.Microsecond
	ExpireTime    int      `json:"expire_time"` // time.Second
	Type          string   `json:"type"`
	MemberCount   int      `json:"member_count"`
	OrgUUID       string   `json:"org_uuid"`
	Workdays      []string `json:"workdays"`
	Workhours     int      `json:"workhours"`
	WorkhoursUnit string   `json:"workhours_unit"`
}

type LoginOrg struct {
	OrgType      int      `json:"org_type"`
	UUID         string   `json:"uuid"`
	Name         string   `json:"name"`
	StyleHash    string   `json:"style_hash"`
	Favicon      string   `json:"favicon"`
	Notification []string `json:"notification"`
}

type LoginResponse struct {
	User  *LoginUser   `json:"user"`
	Teams []*LoginTeam `json:"teams"`
	Org   *LoginOrg    `json:"org"`
}

func Login(email, password string) (loginRespones *LoginResponse, err error) {
	api := "https://api.ones.ai/project/v1/auth/login"
	data := map[string]string{
		"email":    email,
		"password": password,
	}
	b, _ := json.Marshal(data)
	body := bytes.NewReader(b)
	var req *http.Request
	req, err = http.NewRequest(http.MethodPost, api, body)
	if err != nil {
		return
	}
	req.Header.Add("Content-Type", "application/json")
	client := http.DefaultClient
	var resp *http.Response
	resp, err = client.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("Err resp: %v", resp)
		return
	}
	defer resp.Body.Close()
	b, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(b, &loginRespones)
	return
}
