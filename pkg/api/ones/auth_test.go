package ones

import (
	"log"
	"testing"
)

func TestLogin(t *testing.T) {
	resp, err := Login("", "")
	if err != nil {
		t.Error(err)
	} else {
		log.Printf("token: %s", resp.User.Token)
	}
}
