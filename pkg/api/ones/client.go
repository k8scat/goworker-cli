package ones

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	ProductProject = "project"
	ProductWiki    = "wiki"

	// test
	defaultTeamUUID = "RDjYMhKq"
	defaultUserUUID = "5ZPerY1K"
	defaultToken    = "vTyeWrCaOKRD96FGhExrChfekRChscyK2bzyEqLBapWkOYQfnmbSSwKyvV5g5m8m"

	ONESAPITemplate = "https://api.ones.ai/%s/v1/team/%s%s"
)

type Client struct {
	TeamUUID string
	UserUUID string
	Token    string
}

func NewClient(teamUUID, userUUID, token string) (client *Client, err error) {
	if teamUUID == "" || userUUID == "" || token == "" {
		err = errors.New("teamUUID, userUUID and token cannot be empty")
		return
	}
	client = &Client{
		TeamUUID: teamUUID,
		UserUUID: userUUID,
		Token:    token,
	}
	return
}

func (c *Client) Post(product, endpoint string, data interface{}) (raw string, err error) {
	if endpoint == "" {
		err = errors.New("endpoint cannot be empty")
		return
	}
	var body *bytes.Reader
	var b []byte
	if data != nil {
		if b, err = json.Marshal(data); err != nil {
			return
		}
		body = bytes.NewReader(b)
	} else {
		body = nil
	}

	url := fmt.Sprintf(ONESAPITemplate, product, c.TeamUUID, endpoint)
	log.Println(url)
	var req *http.Request
	req, err = http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		return
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Ones-User-ID", c.UserUUID)
	req.Header.Add("Ones-Auth-Token", c.Token)

	var resp *http.Response
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	b, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	raw = string(b)
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("Err resp: %v, body: %s", resp, raw)
		return
	}
	return
}

func (c *Client) Get(product, endpoint string) (raw string, err error) {
	if endpoint == "" {
		err = errors.New("endpoint cannot be empty")
		return
	}
	url := fmt.Sprintf(ONESAPITemplate, product, c.TeamUUID, endpoint)
	var req *http.Request
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Ones-User-ID", c.UserUUID)
	req.Header.Add("Ones-Auth-Token", c.Token)

	var resp *http.Response
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	var b []byte
	b, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	raw = string(b)
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("Err resp: %v, body: %s", resp, raw)
		return
	}
	return
}

func (c *Client) GraphQL(query string) (content string, err error) {
	endpoint := "/items/graphql"
	data := map[string]interface{}{
		"query": query,
	}
	return c.Post(ProductProject, endpoint, data)
}
