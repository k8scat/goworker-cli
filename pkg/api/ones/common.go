package ones

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/k8scat/goworker-cli/internal/config"
)

const (
	FormatWikiPageURL = "https://ones.ai/wiki/#/team/%s/space/%s/page/%s"
	FormatTaskURL     = "https://ones.ai/project/#/team/%s/task/%s"
)

func GenerateUUID(userUUID string) string {
	rand.Seed(time.Now().UnixNano())
	s := "0123456789ABCDEFGHIJKLMNOPQRSTUVXWYZabcdefghijklmnopqrstuvxwyz"
	buf := make([]byte, 8)
	for i := 0; i < 8; i++ {
		buf[i] = s[rand.Intn(len(s))]
	}
	return fmt.Sprintf("%s%s", userUUID, string(buf))
}

func GenerateTaskURL(uuid string) string {
	return fmt.Sprintf(FormatTaskURL, config.GlobalConfig.OnesTeamUUID, uuid)
}

func GenerateWikiURL(spaceUUID, pageUUID string) string {
	return fmt.Sprintf(FormatWikiPageURL, config.GlobalConfig.OnesTeamUUID, spaceUUID, pageUUID)
}
