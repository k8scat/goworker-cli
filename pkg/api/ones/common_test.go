package ones

import (
	"fmt"
	"testing"
)

func TestGenerateUUID(t *testing.T) {
	fmt.Printf("uuid: %s", GenerateUUID(""))
}

func TestGenerateTaskURL(t *testing.T) {
	fmt.Printf("task url: %s\n", GenerateTaskURL("TZxWy57P16qpW58g"))
}
