package ones

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/tidwall/gjson"
)

const (
	FieldUUIDCustomer                = "JrvswW8P" // 客户信息
	FieldUUIDDeliverCustomer         = "PKgB6coJ" // 交付客户信息
	FieldUUIDDeadline                = "field013" // 截止时间
	FieldUUIDProducts                = "field029" // 所属产品
	FieldUUIDAvailabilityEventType   = "SjKqi5At" // 可用性事件类型
	FieldUUIDFeedbackType            = "PjHEiH3d" // 反馈类型
	FieldUUIDOnlineBug               = "field031" // 是否线上缺陷
	FieldUUIDBugAppearStage          = "MtX1sPs6" // BUG引入阶段
	FieldUUIDDeployReportURL         = "BPo72n62" // 实施报告
	FieldUUIDVersionStatus           = "T8GhC4TE" // 版本状态
	FieldUUIDLastDeployDate          = "Y2MuD1tH" // 上次部署日期
	FieldUUIDLastDeployTime          = "YTVSjQXu" // 上次部署时间
	FieldUUIDDeployEngineer          = "TioFkeZn" // 实施工程师
	FieldUUIDMigrateData             = "VK8uFpQv" // 数据迁移
	FieldUUIDDeployTime              = "U1Zf7epq" // 实施时间
	FieldUUIDDeployType              = "N6UcMTLq" // 实施类型
	FieldUUIDDeployMethod            = "PUuiRRim" // 实施方式
	FieldUUIDEstimateManhour         = "field018" // 预估工时
	FieldUUIDPriority                = "field012" // 优先级
	FieldUUIDVersion                 = "PAySkY4n" // 版本号
	FieldUUIDSprint                  = "field011" // 所属迭代
	FieldUUIDCustomerContact         = "WPQBRX7Y" // 对接人姓名
	FieldUUIDCustomerContactPhone    = "QFf9SEK3" // 对接人电话
	FieldUUIDInstanceType            = "6Qek7pDH" // 实例类型
	FieldUUIDTeamUUID                = "NnAcdq9R" // 团队UUID
	FieldUUIDSprintGroup             = "Qr51Lf6j" // 迭代小组
	FieldUUIDAssign                  = "field004" // 负责人
	FieldUUIDOnlineProblem           = "TjdHtvgt" // 线上问题
	FieldUUIDDateBeforeChanged       = "9nCckQGm" // 变更前日期
	FieldUUIDDateAfterChanged        = "nsW3viCy" // 变更后日期
	FieldUUIDSprintStageChanged      = "U759qn3s" // 变更阶段
	FieldUUIDSprintDateChangeType    = "AqhnzbiL" // 变更日期类型
	FieldUUIDSprintStageBeforeChange = "VjUvXCTE" // 变更前阶段

	FieldValueUUIDMigrateDataYes                = "FfLkV6nP" // 是否迁移数据 - 是
	FieldValueUUIDMigrateDataNo                 = "2kCLk4dv" // 是否迁移数据 - 否
	FieldValueUUIDVersionUnstable               = "BTHkKKHR" // 版本状态 - 不稳定版本
	FieldValueUUIDVersionStable                 = "GFBmRZhy" // 版本状态 - 稳定版本
	FieldValueUUIDPrivateDeployInitialInstall   = "DVRBF6SL" // 实施类型 - 私有部署初装
	FieldValueUUIDPrivateDeployUpgrade          = "4NvqH2Lu" // 实施类型 - 私有部署升级
	FieldValueUUIDHostInitialInstall            = "KzDJF3Np" // 实施类型 - 托管初装
	FieldValueUUIDHostUpgrade                   = "X5hs2QYt" // 实施类型 - 托管升级
	FieldValueUUIDRemoteDeploy                  = "PmfgzYsB" // 实施方式 - 远程实施
	FieldValueUUIDProvideInstallPackage         = "N3xRCJzH" // 实施方式 - 提供安装包
	FieldValueUUIDSiteDeploy                    = "EKERTqap" // 实施方式 - 现场实施
	FieldValueUUIDP0                            = "KhZpaAXJ" // 优先级 - P0
	FieldValueUUIDP1                            = "762U6awQ" // 优先级 - P1
	FieldValueUUIDP2                            = "Lv5Tbmih" // 优先级 - P2
	FieldValueUUIDP3                            = "A7UyroWi" // 优先级 - P3
	FieldValueUUIDFeedbackBug                   = "DLMM2DJD" // 反馈类型 - BUG
	FieldValueUUIDFeedbackNewDemand             = "5HiaccVH" // 反馈类型 - 新需求
	FieldValueUUIDFeedbackSystemPerformance     = "2p7KmFtg" // 反馈类型 - 系统性能问题
	FieldValueUUIDFeedbackUsable                = "Jrw6nJ4s" // 反馈类型 - 易用性问题
	FieldValueUUIDFeedbackProduct               = "AdJfghtB" // 反馈类型 - 产品逻辑问题
	FieldValueUUIDFeedbackTechAdvice            = "QFfDBnKc" // 反馈类型 - 技术咨询
	FieldValueUUIDAvailabilityEvent             = "HVXP2tdo" // 可用性事件类型 - 可用性事故
	FieldValueUUIDAvailabilityProblem           = "BQCD28Ux" // 可用性事件类型 - 可用性问题
	FieldValueUUIDOnlineBugYes                  = "TEaU3X2M" // 是否线上缺陷 - 是
	FieldValueUUIDOnlineBugNo                   = "UBKiNeGi" // 是否线上缺陷 - 否
	FieldValueUUIDBugAppearStageFunctionTest    = "V9kkGniu" // BUG引入阶段 - 功能测试
	FieldValueUUIDBugAppearStageIntegrationTest = "Mz2xgGEx" // BUG引入阶段 - 集成测试
	FieldValueUUIDBugAppearStageSystemDemo      = "X5UDpshF" // BUG引入阶段 - 系统演示
	FieldValueUUIDBugAppearStageOnlineValidate  = "HUgSd9Rm" // BUG引入阶段 - 线上验证
	FieldValueUUIDBugAppearStageOnlineFeedback  = "4sa2ykYk" // BUG引入阶段 - 线上反馈
	FieldValueUUIDBugAppearStageOthers          = "S4TXbYGY" // BUG引入阶段 - 其它
	FieldValueUUIDInstanceTypeStandalone        = "CMJr4T4s" // 实例类型 - 单机版
	FieldValueUUIDInstanceTypeHighAvalibility   = "51SKpTvA" // 实例类型 - 高可用

	FieldValueUUIDSprintGroupBaseOne            = "5YGAmZnq" // 基础一组
	FieldValueUUIDSprintGroupBaseTwo            = "JQuoe3X4" // 基础二组
	FieldValueUUIDSprintGroupBaseThree          = "BPy6Txgt" // 基础三组
	FieldValueUUIDSprintGroupBaseFour           = "AFeBNPyq" // 基础四组
	FieldValueUUIDSprintGroupBaseFive           = "GpZ4t7cL" // 基础五组
	FieldValueUUIDSprintGroupBaseSix            = "8FyQP9Jv" // 基础六组
	FieldValueUUIDSprintGroupBaseSeven          = "2nDgMhNB" // 基础七组
	FieldValueUUIDSprintGroupStandardProductOne = "PyaWsx6d" // 标品一组
	FieldValueUUIDSprintGroupStandardProductTwo = "25tXA4Ze" // 标品二组
	FieldValueUUIDSprintGroupDeliverOne         = "NK9vyL4g" // 交付一组
	FieldValueUUIDSprintGroupDeliverTwo         = "UPSQxBFH" // 交付二组
	FieldValueUUIDSprintGroupAvailability       = "V5tUV5rT" // 可用性小组
	FieldValueUUIDSprintGroupDevOps             = "BX8do5NY" // 运维开发
	FieldValueUUIDSprintGroupMobileOne          = "9qhpBgmB" // 移动一组

	FieldValueUUIDSprintStageNotStart        = "QdqE9CXv" // 变更阶段 - 未开始
	FieldValueUUIDSprintStageDemandReview    = "KAE4Ur29" // 变更阶段 - 需求评审
	FieldValueUUIDSprintStagePlan            = "J6e52euL" // 变更阶段 - 迭代计划
	FieldValueUUIDSprintStageDevelop         = "QdgGvt3F" // 变更阶段 - 开发实现
	FieldValueUUIDSprintStageFuncTest        = "SG39uLzN" // 变更阶段 - 功能测试及验收
	FieldValueUUIDSprintStageIntegrationTest = "2vNYLK8D" // 变更阶段 - 集成测试
	FieldValueUUIDSprintStageSystemDemo      = "4mJ8Y2i1" // 变更阶段 - 系统演示
	FieldValueUUIDSprintStageDeploy          = "G1qsgQxP" // 变更阶段 - 发布上线
	FieldValueUUIDSprintStageDone            = "Wuku9PkR" // 变更阶段 - 已完成

	FieldValueUUIDSprintDatePlanStart = "KMHJoSxo" // 变更日期类型 - 计划开始日期
	FieldValueUUIDSprintDatePlanEnd   = "S6Q6bLmg" // 变更日期类型 - 计划完成日期

	FieldValueUUIDOnlineProblemYes = "WTZb3PZx" // 线上问题 - 是
	FieldValueUUIDOnlineProblemNo  = "NAuFMgGG" // 线上问题 - 否

	ManhourBase = 100000

	DefaultFieldOptionBackgroundColor = "#e63422"
	DefaultFieldOptionColor           = "#fff"
)

type Field struct {
	UUID         string         `json:"uuid"`
	Name         string         `json:"name"`
	NamePinyin   string         `json:"namePinyin"`
	Type         int            `json:"type"`
	DefaultValue interface{}    `json:"defaultValue"`
	CreateTime   int64          `json:"createTime"` // time.Second
	BuildIn      bool           `json:"builtIn"`
	Options      []*FieldOption `json:"options"`
}

type FieldOption struct {
	UUID            string `json:"uuid"`
	Value           string `json:"value"`
	BackgroundColor string `json:"bgColor"`
	Color           string `json:"color"`
	Desc            string `json:"description"`
	DefaultSelected bool   `json:"defaultSelected"`
}

func (c *Client) GetFieldByUUID(fieldUUID string) (field *Field, err error) {
	query := `
{
    fields (
        filter: {
            uuid_equal: "%s",
            pool_in: ["task"],
            context: {
				type_equal: "team"
			}
        }
    ) {
		uuid
		name
		namePinyin
		type
		defaultValue
		createTime
		builtIn
        options {
            uuid
            value
			bgColor
			color
			defaultSelected
        }
    }
}`
	query = fmt.Sprintf(query, fieldUUID)
	var content string

	if content, err = c.GraphQL(query); err != nil {
		return
	}
	if len(gjson.Get(content, "data.fields").Array()) == 0 {
		err = fmt.Errorf("field not found, uuid=%s", fieldUUID)
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.fields.0").String()), &field)
	return
}

func (c *Client) UpdateField(field map[string]interface{}) error {
	if field["uuid"] == nil || field["uuid"] == "" {
		return errors.New("field uuid cannot be empty")
	}
	endpoint := fmt.Sprintf("/field/%s/update", field["uuid"])
	data := map[string]interface{}{
		"field": field,
	}
	_, err := c.Post(ProductProject, endpoint, data)
	return err
}
