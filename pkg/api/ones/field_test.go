package ones

import (
	"log"
	"testing"
)

func TestGetFieldByUUID(t *testing.T) {
	setup()
	customer, _ := testClient.GetFieldByUUID(FieldUUIDCustomer)
	deliverCustomer, _ := testClient.GetFieldByUUID(FieldUUIDDeliverCustomer)

	customerMap := make(map[string]bool)
	for _, customerOption := range customer.Options {
		if _, exists := customerMap[customerOption.Value]; exists {
			log.Println(customerOption.Value)
		} else {
			customerMap[customerOption.Value] = true
		}
	}
	log.Println(len(customerMap))

	deliverCustomerMap := make(map[string]bool)
	for _, deliverCustomerOption := range deliverCustomer.Options {
		if deliverCustomerOption.Value == "联通智网科技有限公司" {
			log.Println("联通智网科技有限公司")
		}
		deliverCustomerMap[deliverCustomerOption.Value] = true
	}
	log.Println(len(deliverCustomerMap))

	for deliverCustomerName := range deliverCustomerMap {
		if _, exists := customerMap[deliverCustomerName]; !exists {
			log.Println(deliverCustomerName)
		}
	}
}

func TestUpdateField(t *testing.T) {
	setup()
	field := map[string]interface{}{
		"uuid": "H9KbkoGm",
		"options": []map[string]interface{}{
			{
				"uuid":             "32410042",
				"value":            "测试23",
				"background_color": "#e63422",
				"color":            "#fff",
			},
		},
	}
	if err := testClient.UpdateField(field); err != nil {
		t.Error(err)
	}
}
