package ones

import (
	"encoding/json"
	"fmt"

	"github.com/tidwall/gjson"
)

type Manhour struct {
	UUID        string `json:"uuid"`
	Task        *Task  `json:"task"`
	Description string `json:"description"`
	Hours       int    `json:"hours"`
	Owner       *User  `json:"owner"`
	Key         string `json:"key"`
	CreateTime  int    `json:"createTime"` // time.Second
	StartTime   int    `json:"startTime"`  // time.Second
}

func (c *Client) ListManhoursBySprintAndOwner(sprintUUID, ownerUUID string) (manhours []*Manhour, err error) {
	query := `
	{
		manhours(
			filter: {
				sprint_in: ["%s"]
				owner_in: ["%s"]
			}
		) {
			uuid
			hours
			key
			description
			createTime
			startTime
			task {
				name
				uuid
			}
			owner {
				uuid
				name
			}
		}
	}`
	query = fmt.Sprintf(query, sprintUUID, ownerUUID)
	var content string
	if content, err = c.GraphQL(query); err != nil {
		return
	}
	if len(gjson.Get(content, "data.manhours").Array()) == 0 {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.manhours").String()), &manhours)
	return
}
