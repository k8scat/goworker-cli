package ones

import (
	"log"
	"testing"
)

func TestListManhoursBySprintAndOwner(t *testing.T) {
	setup()
	sprintUUID := "G2APmNzn"
	ownerUUID := "GSRCM5Xd"
	manhours, err := testClient.ListManhoursBySprintAndOwner(sprintUUID, ownerUUID)
	if err != nil {
		t.Error(err)
	} else {
		for _, mh := range manhours {
			log.Println(mh.UUID)
		}
	}
}
