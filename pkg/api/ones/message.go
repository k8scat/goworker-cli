package ones

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/tidwall/gjson"
)

const (
	MessageTypeSystemLabel     = "system"
	MessageTypeDiscussionLabel = "discussion"
)

type Message struct {
	UUID                  string      `json:"uuid"`
	TeamUUID              string      `json:"team_uuid"`
	RefType               string      `json:"ref_type"`
	RefID                 string      `json:"ref_id"`
	Type                  string      `json:"type"`
	From                  string      `json:"from"`
	To                    string      `json:"to"`
	SendTime              int64       `json:"send_time"` // time.Microsecond
	SubjectType           string      `json:"subject_type"`
	SubjectID             string      `json:"subject_id"`
	Action                string      `json:"action"`
	ObjectType            string      `json:"object_type"`
	ObjectID              string      `json:"object_id"`
	ObjectName            string      `json:"object_name"`
	ObjectAttr            string      `json:"object_attr"`
	OldValue              string      `json:"old_value"`
	NewValue              string      `json:"new_value"`
	Ext                   *MessageExt `json:"ext"`
	IsCanShowRichtextDiff bool        `json:"is_can_show_richtext_diff"`
	Text                  string      `json:"text"`
}

type MessageExt struct {
	UUID      string       `json:"uuid"`
	Name      string       `json:"name"`
	FieldName string       `json:"field_name"`
	FieldType int          `json:"field_type"`
	FieldUUID string       `json:"field_uuid"`
	NewOption *FieldOption `json:"new_option"`
	NewValue  string       `json:"new_value"`
	OldOption *FieldOption `json:"old_option"`
	OldValue  string       `json:"old_value"`
}

func (c *Client) SendMessage(taskUUID string, content string) error {
	if content == "" {
		return errors.New("content cannot be empty")
	}
	endpoint := fmt.Sprintf("/task/%s/send_message", taskUUID)
	data := map[string]interface{}{
		"text": content,
		"uuid": GenerateUUID(""),
	}
	result, err := c.Post(ProductProject, endpoint, data)
	if err != nil {
		return err
	}
	if gjson.Get(result, "code").Int() != 200 {
		return fmt.Errorf("send message failed, err=%s", content)
	}
	return err
}

func (c *Client) ListMessages(taskUUID string) (messages []*Message, err error) {
	endpoint := fmt.Sprintf("/task/%s/messages", taskUUID)
	var content string
	if content, err = c.Get(ProductProject, endpoint); err != nil {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "messages").String()), &messages)
	return
}
