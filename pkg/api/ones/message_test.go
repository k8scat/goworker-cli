package ones

import (
	"log"
	"testing"
)

func TestSendMessage(t *testing.T) {
	setup()
	taskUUID := "TZxWy57P16qpW58g"
	content := "test message"
	if err := testClient.SendMessage(taskUUID, content); err != nil {
		t.Error(err)
	}
}

func TestListMessages(t *testing.T) {
	setup()
	taskUUID := "TZxWy57P16qpW58g"
	messages, err := testClient.ListMessages(taskUUID)
	if err != nil {
		t.Error(err)
	} else {
		for _, message := range messages {
			log.Println(message.UUID)
		}
	}
}
