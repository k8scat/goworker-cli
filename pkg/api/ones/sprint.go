package ones

import (
	"encoding/json"

	"github.com/tidwall/gjson"
)

const (
	SprintStatusNotStart   = 1
	SprintStatusInProgress = 2
	SprintStatusDone       = 3
	SprintStatusDeleted    = 4
)

type SprintPayload struct {
	UUID        string                      `json:"uuid"`
	Title       string                      `json:"title"`
	ProjectUUID string                      `json:"project_uuid"`
	TitlePinYin string                      `json:"title_pinyin"`
	Description string                      `json:"description"`
	StartTime   int64                       `json:"start_time"`
	EndTime     int64                       `json:"end_time"`
	Status      int                         `json:"status"`
	CreateTime  int64                       `json:"create_time"`
	IsPin       bool                        `json:"is_pin"`
	IsOpenGantt bool                        `json:"is_open_gantt"`
	DailyHours  int                         `json:"daily_hours"`
	Assign      string                      `json:"assign"`
	Goal        string                      `json:"goal"`
	Period      string                      `json:"period"`
	Fields      []*SprintFieldValuePayload  `json:"fields"`
	Statuses    []*SprintStatusValuePayload `json:"statuses"`
	Progress    int64                       `json:"progress"`
}

type SprintFieldValuePayload struct {
	FieldUUID string      `json:"field_uuid"`
	Type      string      `json:"type"`
	Name      string      `json:"name"`
	Value     interface{} `json:"value"`
}

type SprintStatusValuePayload struct {
	UUID            string `json:"status_uuid"`
	Name            string `json:"name"`
	Category        string `json:"category"`
	PlanStartTime   int64  `json:"plan_start_time"`
	PlanEndTime     int64  `json:"plan_end_time"`
	ActualStartTime int64  `json:"actual_start_time"`
	ActualEndTime   int64  `json:"actual_end_time"`
	IsCurrentStatus bool   `json:"is_current_status"`
	DescPlain       string `json:"desc_plain"`
	DescRich        string `json:"desc_rich"`
}

func (c *Client) ListSprintsByTeam(teamUUID string) (sprints []*SprintPayload, err error) {
	endpoint := "/sprints/all"
	var content string
	if content, err = c.Get(ProductProject, endpoint); err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "sprints").String()), &sprints)
	return
}
