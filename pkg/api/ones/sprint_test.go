package ones

import (
	"log"
	"testing"
)

func TestListSprintsByTeam(t *testing.T) {
	setup()
	sprints, err := testClient.ListSprintsByTeam(defaultTeamUUID)
	if err != nil {
		t.Error(err)
	} else {
		for _, sprint := range sprints {
			log.Printf("%+v", sprint)
		}
	}
}
