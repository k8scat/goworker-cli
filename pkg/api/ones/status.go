package ones

import (
	"encoding/json"

	"github.com/tidwall/gjson"
)

const (
	StatusUUIDApplyDevops                = "6DW1Szdk" // 申请转运维
	StatusUUIDApplyProduct               = "5G46JjPa" // 申请转产品
	StatusUUIDApplyDeliver               = "3om1gq2F" // 申请转交付
	StatusUUIDApplyStandardProduct       = "FdGUfbbT" // 申请转标品
	StatusUUIDApplyAvailability          = "FFDQcGx4" // 申请转可用性
	StatusUUIDTransferredDevops          = "VxyVG2Tc" // 已转运维
	StatusUUIDTransferredProduct         = "LGe4Hrmp" // 已转产品
	StatusUUIDTransferredDeliver         = "UQDeRWKr" // 已转交付
	StatusUUIDTransferredStandardProduct = "QHzVtUBr" // 已转标品
	StatusUUIDTransferredAvailability    = "2rU26mcD" // 已转可用性
	StatusUUIDDone                       = "LS1jaMvi" // 已完成
	StatusUUIDValidatePass               = "JGfzuQYK" // 验证通过
	StatusUUIDValidateNotPass            = "7te9pzDe" // 验证不通过
	StatusUUIDReleased                   = "DieosZBa" // 已发布
	StatusUUIDRunning                    = "Gj99kA71" // 运行中
	StatusUUIDNotStart                   = "ARdjBiAo" // 未开始
	StatusUUIDUpgradeInfoCollect         = "Ug57AaVD" // 升级信息收集
	StatusUUIDNotDeploy                  = "PrZYXuEg" // 未部署
	StatusUUIDDelivered                  = "THoVHhAo" // 已交付
	StatusUUIDStatus                     = "field005" // 状态
	StatusUUIDSaasDeployed               = "SRrEGrTV" // 已上线SaaS
	StatusUUIDApproving                  = "TP3Kv7jJ" // 待审批
	StatusUUIDPrivateInstanceNotDeployed = "PrZYXuEg" // 私有云实例未部署
	StatusUUIDPrivateInstanceRunning     = "PrZYXuEg" // 私有云实例运行中
	StatusUUIDPrivateInstanceUpgrading   = "M6c2Muyw" // 待实例升级
	StatusUUIDClose                      = "QXSKmnQQ" // 关闭
	StatusUUIDClosed                     = "V9HLc348" // 已关闭
	StatusUUIDApplyTransfer              = "Hdcpjf5J" // 申请转出

	StatusCategoryToDo       = "to_do"
	StatusCategoryInProgress = "in_progress"
	StatusCategoryDone       = "done"

	StatusNameDone           = "已完成"
	StatusNameDeployAccident = "实施事故"
	StatusNameSprintAccident = "迭代事故"
)

type Status struct {
	UUID       string `json:"uuid"`
	Name       string `json:"name"`
	NamePinyin string `json:"name_pinyin"`
	Category   string `json:"category"`
	BuildIn    bool   `json:"build_in"`
	CreateTime int64  `json:"create_time"` // time.Second
}

func (c *Client) ListTaskStatuses() (statuses []*Status, err error) {
	endpoint := "/task_statuses"
	var content string
	if content, err = c.Get(ProductProject, endpoint); err != nil {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "task_statuses").String()), &statuses)
	return
}
