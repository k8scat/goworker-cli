package ones

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestListTaskStatuses(t *testing.T) {
	setup()
	statuses, err := testClient.ListTaskStatuses()
	if err != nil {
		t.Error(err)
	} else {
		for _, status := range statuses {
			if status.Name == "" || status.UUID == "XSHbGvpt" {
				b, _ := json.Marshal(status)
				fmt.Println(string(b))
				break
			}
		}
	}
}
