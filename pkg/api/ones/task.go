package ones

import (
	"encoding/json"
	"fmt"

	"github.com/tidwall/gjson"
)

const (
	ProjectUUIDDevOps                    = "GL3ysesF3hj2At3c" // 运维开发
	ProjectUUIDDesk                      = "GL3ysesFPdnAQNIU" // ONES Desk
	ProjectUUIDDeliverGroup              = "9eTpLRF6HSoQK7S1" // 交付组
	ProjectUUIDAvailabilityManage        = "YEL8b4LVqaWIshd9" // 可用性管理
	ProjectUUIDStandardProductGroup      = "DgdrtjQHrANIRmOg" // 标品组
	ProjectUUIDPrivateInstanceManagement = "GL3ysesF59l5lRH9" // 私有云实例管理
	ProjectUUIDReleaseManagement         = "YEL8b4LVgWM3n6BK" // 发布管理

	ProjectUUIDGroupBaseOne              = "KuZfE9scT2ca5BXF" // 基础一组
	ProjectUUIDGroupBaseTwoAndSix        = "GL3ysesF3DpZTVUS" // 基础二组/基础六组
	ProjectUUIDGroupBaseThreeAndSeven    = "KuZfE9scjSrGgi6r" // 基础三组/基础七组
	ProjectUUIDGroupBaseFourAndMobileOne = "KuZfE9sclrSg9E6c" // 基础四组/移动一组
	ProjectUUIDGroupBaseFive             = "MUzCtgRc6JHQPer5" // 基础五组
	ProjectUUIDGroupDeliverOne           = "HAbxwpBC5z3T7UkG" // 交付一组
	ProjectUUIDGroupDeliverTwo           = "HAbxwpBCAjJN52Qi" // 交付二组

	IssueTypeUUIDBacklog                = "Sw6adY2M" // Backlog
	IssueTypeUUIDInsideFeedback         = "B6LY9hdu" // 内部反馈
	IssueTypeUUIDAvailabilityEvent      = "PtFbdA4a" // 可用性事件
	IssueTypeUUIDDemandEvaluate         = "UC7vpRYK" // 需求评估
	IssueTypeUUIDTask                   = "C7qX3XoF" // 任务
	IssueTypeUUIDBug                    = "UorcXCjz" // BUG
	IssueTypeUUIDWorkOrder              = "7sxvwZMY" // 工单
	IssueTypeUUIDPrivateInstance        = "GvyPHeW5" // 私有云实例
	IssueTypeUUIDDeployApply            = "4f89rNoy" // 实施申请
	IssueTypeUUIDDeployApplyTemplate    = "XLaRuQq8" // 实施申请模板
	IssueTypeUUIDDemand                 = "MZ5U1oBz" // 需求
	IssueTypeUUIDSubDeployTask          = "DCGB6ZQv" // 子实施任务
	IssueTypeUUIDReleaseApply           = "Fd7VdS9X" // 发布申请
	IssueTypeUUIDSubReleaseApply        = "KA1QspxB" // 子发布申请
	IssueTypeUUIDDeliverable            = "3bLHbgnu" // 交付物
	IssueTypeUUIDOpsService             = "UQQpmFBi" // 运维服务
	IssueTypeUUIDDeskBug                = "FpFALpKJ" // DESK转入Bug
	IssueTypeUUIDSprintPlanChangeRecord = "AxSZzwaU" // 迭代计划变更
)

type Task struct {
	UUID            string       `json:"uuid"`
	Name            string       `json:"name"`
	Customer        *FieldOption `json:"_JrvswW8P"`
	LicenseDeadline int64        `json:"_BKWi3Fce"` // removed
	DeployEngineer  *User        `json:"_TioFkeZn"`
	LastDeployDate  int64        `json:"_Y2MuD1tH"`  // time.Second
	LastDeployTime  int64        `json:"_YTVSjQXu"`  // time.Second
	CreateTime      int64        `json:"createTime"` // time.Microsecond
	DeployTime      int64        `json:"_U1Zf7epq"`  // time.Second
	SubTasks        []*Task      `json:"subTasks"`
	Links           []*Link      `json:"links"`
	Parent          *Task        `json:"parent"`
	RelatedTasks    []*Task      `json:"relatedTasks"`
	VersionStatus   *FieldOption `json:"_T8GhC4TE"`
	Description     string       `json:"description"`
	Owner           *User        `json:"owner"`
	Watchers        []*User      `json:"watchers"`
	Assign          *User        `json:"assign"`
	Status          *Status      `json:"status"`
	Priority        *FieldOption `json:"priority"`
	FeedbackType    *FieldOption `json:"_PjHEiH3d"`
	Products        []*Field     `json:"products"`
	Manhours        []*Manhour   `json:"manhours"`
	DeployType      *FieldOption `json:"_N6UcMTLq"`
	DeployReportUrl string       `json:"_BPo72n62"`
	CustomerContact string       `json:"_WPQBRX7Y"`
	Number          int          `json:"number"`
	Deadline        int64        `json:"deadline"`
	MigrateData     *FieldOption `json:"_VK8uFpQv"`
	SaleAssign      *User        `json:"_MWFJWBnG"`
	IssueType       *Field       `json:"issueType"`
	DeployMethod    *FieldOption `json:"_PUuiRRim"`
	Version         string       `json:"_PAySkY4n"`
	SubIssueType    *Field       `json:"subIssueType"`
	Sprint          *Field       `json:"sprint"`
	TeamUUID        string       `json:"_NnAcdq9R"`
	SprintGroup     *FieldOption `json:"_Qr51Lf6j"`
}

type FieldValue struct {
	FieldUUID string      `json:"field_uuid"`
	Type      *int        `json:"type"` // 在创建工作项的时候, Type不是必要的
	Value     interface{} `json:"value"`
}

type Link struct {
	TaskUUID         string `json:"taskUUID"`
	TaskLinkTypeUUID string `json:"taskLinkTypeUUID"`
	LinkDescType     string `json:"linkDescType"`
}

type RelatedTasks struct {
	TaskUUIDs        []string `json:"task_uuids"`
	TaskLinkTypeUUID string   `json:"task_link_type_uuid"`
	LinkDescType     string   `json:"link_desc_type"`
}

type TaskPayload struct {
	Assign           string        `json:"assign"`
	DescRich         string        `json:"desc_rich"`
	FieldValues      []*FieldValue `json:"field_values"`
	IssueTypeUUID    string        `json:"issue_type_uuid"`
	SubIssueTypeUUID string        `json:"sub_issue_type_uuid"` // 创建子工作项时, IssueTypeUUID应该和SubIssueTypeUUID相同
	ParentUUID       string        `json:"parent_uuid"`
	Priority         string        `json:"priority"` // 创建子工作项时必填
	ProjectUUID      string        `json:"project_uuid"`
	Summary          string        `json:"summary"`
	UUID             string        `json:"uuid"`
	Watchers         []string      `json:"watchers"`
}

func (c *Client) UpdateTasks(tasks []map[string]interface{}) error {
	endpoint := "/tasks/update3"
	data := map[string]interface{}{
		"tasks": tasks,
	}
	content, err := c.Post(ProductProject, endpoint, data)
	if err != nil {
		return err
	}
	if len(gjson.Get(content, "bad_tasks").Array()) > 0 {
		return fmt.Errorf("update tasks failed, err=%s", content)
	}
	return nil
}

func (c *Client) AddTasks(tasks []*TaskPayload) error {
	endpoint := "/tasks/add2"
	data := map[string]interface{}{
		"tasks": tasks,
	}
	content, err := c.Post(ProductProject, endpoint, data)
	if err != nil {
		return err
	}
	if len(gjson.Get(content, "bad_tasks").Array()) > 0 {
		return fmt.Errorf("add tasks failed, err=%s", content)
	}
	return nil
}

func (c *Client) AddWatchers(taskUUID string, watcherUUIDs []string) error {
	endpoint := fmt.Sprintf("/task/%s/watchers/add", taskUUID)
	data := map[string]interface{}{
		"watchers": watcherUUIDs,
	}
	content, err := c.Post(ProductProject, endpoint, data)
	if err != nil {
		return err
	}
	if gjson.Get(content, "code").Int() != 200 {
		return fmt.Errorf("add watchers failed, err=%s", content)
	}
	return nil
}

func (c *Client) GetTaskByName(projectUUID, issueTypeUUID, name string) (task *Task, err error) {
	query := `
{
    tasks(
        filter: {
            project_in: ["%s"]
            issueType_in: ["%s"]
            name_equal: "%s"
        }
    ) {
        uuid
        name
    }
}`
	query = fmt.Sprintf(query, projectUUID, issueTypeUUID, name)
	var content string
	content, err = c.GraphQL(query)
	if err != nil {
		return
	}
	if len(gjson.Get(content, "data.tasks").Array()) == 0 {
		err = fmt.Errorf("task not found, project_uuid=%s, issue_type_uuid=%s, name=%s", projectUUID, issueTypeUUID, name)
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.tasks.0").String()), &task)
	return
}

func (c *Client) GetTaskByUUID(uuid string) (task *Task, err error) {
	query := `
{
    tasks(
        filter: {
            uuid_equal: "%s"
        }
    ) {
        uuid
		name
		createTime
		description
		status {
			uuid
			name
			category
		}
		issueType {
			uuid
			name
		}
		subIssueType {
			uuid
			name
		}
		links {
			taskLinkTypeUUID
            taskUUID
            linkDescType
		}
		relatedTasks {
			uuid
			name
			createTime
			description
			status {
				uuid
				name
				category
			}
			issueType {
				uuid
				name
			}
			subIssueType {
				uuid
				name
			}
		}
		subTasks {
			uuid
			name
			description
		}
		_%s {
			uuid
			value
		}
		_%s {
			uuid
			name
		}
		_%s
		_%s
		_%s
		_%s
    }
}`
	query = fmt.Sprintf(query, uuid, FieldUUIDDeployMethod, FieldUUIDDeployEngineer,
		FieldUUIDDeployTime, FieldUUIDTeamUUID, FieldUUIDVersion, FieldUUIDLastDeployDate)
	var content string
	content, err = c.GraphQL(query)
	if err != nil {
		return
	}
	if len(gjson.Get(content, "data.tasks").Array()) == 0 {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.tasks.0").String()), &task)
	return
}

func (c *Client) AddRelatedTasks(taskUUID string, taskLinkTypeUUID, linkDescType string, taskUUIDs []string) error {
	relatedTasks := &RelatedTasks{
		TaskLinkTypeUUID: taskLinkTypeUUID,
		LinkDescType:     linkDescType,
		TaskUUIDs:        taskUUIDs,
	}
	endpoint := fmt.Sprintf("/task/%s/related_tasks", taskUUID)
	content, err := c.Post(ProductProject, endpoint, relatedTasks)
	if err != nil {
		return err
	}
	if len(gjson.Get(content, "error_relate_tasks").Array()) > 0 {
		return fmt.Errorf("add related tasks failed, err=%s", content)
	}
	return nil
}

func (c *Client) DeleteRelatedTasks(taskUUID string, taskLinkTypeUUID, linkDescType string, taskUUIDs []string) error {
	relatedTasks := &RelatedTasks{
		TaskLinkTypeUUID: taskLinkTypeUUID,
		LinkDescType:     linkDescType,
		TaskUUIDs:        taskUUIDs,
	}
	endpoint := fmt.Sprintf("/task/%s/delete/related_tasks", taskUUID)
	content, err := c.Post(ProductProject, endpoint, relatedTasks)
	if err != nil {
		return err
	}
	if len(gjson.Get(content, "error_relate_tasks").Array()) > 0 {
		return fmt.Errorf("delete related tasks failed, err=%s", content)
	}
	return nil
}

func (c *Client) CopyTask(taskUUID string, targetProjectUUID, targetIssueTypeUUID string) (newTaskUUID string, err error) {
	endpoint := fmt.Sprintf("/task/%s/copy", taskUUID)
	data := map[string]interface{}{
		"project_uuid":    targetProjectUUID,
		"issue_type_uuid": targetIssueTypeUUID,
	}
	var content string
	content, err = c.Post(ProductProject, endpoint, data)
	if err != nil {
		return
	}
	newTaskUUID = gjson.Get(content, "task.uuid").String()
	return
}

func (c *Client) ListSubTasks(taskUUID string) (subTasks []*Task, err error) {
	query := `
{
	tasks(
		filter: {
		    uuid_equal: "%s"
		}
	) {
		uuid
		name
	    subTasks{
	        uuid
	        name
	    }
	}
}`
	query = fmt.Sprintf(query, taskUUID)
	var content string
	if content, err = c.GraphQL(query); err != nil {
		return
	}
	if len(gjson.Get(content, "data.tasks").Array()) == 0 {
		err = fmt.Errorf("task not found, uuid=%s", taskUUID)
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.tasks.0.subTasks").String()), &subTasks)
	return
}

func (c *Client) UpdateAssessManhour(taskUUID string, manhours float64) error {
	endpoint := fmt.Sprintf("/task/%s/assess_manhour/update", taskUUID)
	data := map[string]int{
		"value": int(manhours * ManhourBase),
	}
	content, err := c.Post(ProductProject, endpoint, data)
	if err != nil {
		return err
	}
	if gjson.Get(content, "code").Int() != 200 {
		return fmt.Errorf("update assess manhour failed, err=%s", content)
	}
	return nil
}
