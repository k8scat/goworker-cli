package ones

import (
	"fmt"
	"testing"
)

func TestCopyTask(t *testing.T) {
	setup()
	templateTaskUUID := "5ZPerY1KjgD7h0xL"
	taskUUID, err := testClient.CopyTask(templateTaskUUID, ProjectUUIDReleaseManagement, IssueTypeUUIDDeployApply)
	if err != nil {
		t.Error(err)
	} else {
		fmt.Println(taskUUID)
	}
}

func TestListSubTasks(t *testing.T) {
	setup()
	taskUUID := "5ZPerY1KszoGyE0d"
	subTasks, err := testClient.ListSubTasks(taskUUID)
	if err != nil {
		t.Error(err)
	}
	for _, subTask := range subTasks {
		fmt.Println(subTask.Name)
	}
}

func TestGetTaskByUUID(t *testing.T) {
	client, _ := NewClient(defaultTeamUUID, defaultUserUUID, defaultToken)
	taskUUID := "5ZPerY1KszoGyE0d"
	task, err := client.GetTaskByUUID(taskUUID)
	if err != nil {
		t.Error(err)
	} else {
		fmt.Println(task.Name)
	}
}

func TestAddTasks(t *testing.T) {
	setup()
	tasks := []*TaskPayload{
		{
			UUID:          GenerateUUID(UserUUIDLan),
			ProjectUUID:   ProjectUUIDReleaseManagement,
			IssueTypeUUID: IssueTypeUUIDDeployApply,
			Summary:       "Test deploy apply",
			DescRich:      "this is a rich desc",
			Priority:      FieldValueUUIDP2,
			Assign:        UserUUIDLan,
			FieldValues: []*FieldValue{
				{
					FieldUUID: FieldUUIDDeployType,
					Value:     FieldValueUUIDPrivateDeployInitialInstall,
				},
			},
		},
	}
	if err := testClient.AddTasks(tasks); err != nil {
		t.Error(err)
	}
}
