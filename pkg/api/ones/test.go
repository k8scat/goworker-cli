package ones

var testClient *Client

func setup() {
	var err error
	if testClient, err = NewClient(defaultTeamUUID, defaultUserUUID, defaultToken); err != nil {
		panic(err)
	}
}
