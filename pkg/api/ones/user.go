package ones

import (
	"encoding/json"
	"fmt"

	"github.com/tidwall/gjson"
)

const (
	UserUUIDZouYongjin = "GL3ysesF"
	UserUUIDWangJiehui = "CdWRT5v6"
	UserUUIDWanHuasong = "GSRCM5Xd"
	UserUUIDLan        = "5ZPerY1K"

	UserStatusNormal  = "normal"
	UserStatusDeleted = "deleted"
)

type User struct {
	UUID       string `json:"uuid"`
	Status     string `json:"status"`
	Name       string `json:"name"`
	NamePinyin string `json:"name_pinyin"`
	Title      string `json:"title"`
	Avatar     string `json:"avatar"`
	Email      string `json:"email"`
}

func (c *Client) ListNormalUsersByName(name string) (users []*User, err error) {
	query := `
{
    users(
        filter: {
            name_equal: "%s",
            status_equal: "normal"
    }) {
		uuid
		status
		name
    }
}`
	query = fmt.Sprintf(query, name)
	var content string
	content, err = c.GraphQL(query)
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.users").String()), &users)
	return
}

func (c *Client) GetUserByUUID(userUUID string) (user *User, err error) {
	query := `
{
    users(
        filter: {
            uuid_equal: "%s"
	}) {
		uuid
		name
        status
    }
}`
	query = fmt.Sprintf(query, userUUID)
	var content string
	content, err = c.GraphQL(query)
	if err != nil {
		return
	}
	if len(gjson.Get(content, "data.users").Array()) == 0 {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.users.0").String()), &user)
	return
}

func (c *Client) GetUserByName(name string) (user *User, err error) {
	query := `
	{
		users(
			filter: {
				name_equal: "%s"
				status_equal: "normal"
		}) {
			uuid
			name
			status
		}
	}`
	query = fmt.Sprintf(query, name)
	var content string
	content, err = c.GraphQL(query)
	if err != nil {
		return
	}
	if len(gjson.Get(content, "data.users").Array()) == 0 {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "data.users.0").String()), &user)
	return
}
