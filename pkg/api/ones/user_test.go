package ones

import (
	"log"
	"testing"
)

func TestGetUserByName(t *testing.T) {
	setup()
	user, err := testClient.GetUserByName("万华松")
	if err != nil {
		t.Error(err)
	} else {
		if user != nil {
			log.Println(user.UUID)
		} else {
			log.Println("user not found")
		}

	}
}
