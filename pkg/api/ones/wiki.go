package ones

import (
	"encoding/json"
	"fmt"

	"github.com/tidwall/gjson"
)

const (
	DraftStatusSpace     = 1
	DraftStatusPage      = 2
	DraftStatusPublished = 3

	CopySrcTypeTemplate = "template"
	CopySrcTypePage     = "page"

	SpaceUUIDServiceDepartment  = "U4DDUdLm" // 服务部
	SpaceUUIDCustomerManagement = "TF34tMF1" // 客户跟进管理
)

type Draft struct {
	UUID       string `json:"uuid"`
	SpaceUUID  string `json:"space_uuid"`
	PageUUID   string `json:"page_uuid"`
	OwnerUUID  string `json:"owner_uuid"`
	Status     int    `json:"status"`
	CreateTime int    `json:"create_time"`
}

type Page struct {
	UUID                 string `json:"uuid"`
	Title                string `json:"title"`
	ParentUUID           string `json:"parent_uuid"`
	SpaceUUID            string `json:"space_uuid"`
	LastModifiedTime     int64  `json:"last_modified_time"`
	LastModifiedUserUUID string `json:"last_modified_user_uuid"`
}

func (c *Client) CreateDraftByCopy(spaceUUID, copySrcType, copySrcUUID, title, pageUUID string) (draft *Draft, err error) {
	endpoint := fmt.Sprintf("/space/%s/drafts/add", spaceUUID)
	data := map[string]interface{}{
		"copy_src_type": copySrcType,
		"copy_src_uuid": copySrcUUID,
		"page_uuid":     pageUUID,
		"title":         title,
		"status":        DraftStatusSpace,
	}
	var content string
	content, err = c.Post(ProductWiki, endpoint, data)
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(content), draft)
	return
}

func (c *Client) PublishDraft(draft *Draft, title, content string) (pageUUID string, err error) {
	endpoint := fmt.Sprintf("/space/%s/draft/%s/update", draft.SpaceUUID, draft.UUID)
	data := map[string]interface{}{
		"uuid":         draft.UUID,
		"space_uuid":   draft.SpaceUUID,
		"content":      content,
		"title":        title,
		"page_uuid":    draft.PageUUID,
		"status":       draft.Status,
		"from_version": -1,
		"is_published": true,
	}
	var respContent string
	respContent, err = c.Post(ProductWiki, endpoint, data)
	if err != nil {
		return
	}
	pageUUID = gjson.Get(respContent, "page_uuid").String()
	return
}

func (c *Client) CreateDraft(spaceUUID, pageUUID, title, content string, status int) (draft *Draft, err error) {
	if spaceUUID == "" {
		err = fmt.Errorf("space_uuid cannot be empty")
		return
	}
	endpoint := fmt.Sprintf("/space/%s/drafts/add", spaceUUID)
	data := map[string]interface{}{
		"page_uuid": pageUUID,
		"title":     title,
		"content":   content,
		"status":    status,
	}
	var respContent string
	respContent, err = c.Post(ProductWiki, endpoint, data)
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(respContent), &draft)
	return
}

func (c *Client) CreatePage(spaceUUID, parentPageUUID, title, content string) (pageUUID string, err error) {
	var draft *Draft
	if draft, err = c.CreateDraft(spaceUUID, parentPageUUID, title, content, DraftStatusSpace); err != nil {
		return
	}
	pageUUID, err = c.PublishDraft(draft, title, content)
	return
}

// TODO
func GetPageUUIDByParentPageUUIDAndTitle(client *Client, spaceUUID, parentPageUUID, title string) (pageUUID string, err error) {
	endpoint := fmt.Sprintf("/space/%s/pages", spaceUUID)
	var content string
	content, err = client.Post(ProductWiki, endpoint, nil)
	if err != nil {
		return
	}
	var pages []*Page
	err = json.Unmarshal([]byte(gjson.Get(content, "pages").String()), &pages)
	if err != nil {
		return
	}
	for _, page := range pages {
		if page.ParentUUID == parentPageUUID && page.Title == title {
			parentPageUUID = page.UUID
			return
		}
	}
	return
}

func (c *Client) ListPagesBySpaceUUID(spaceUUID string) (pages []*Page, err error) {
	endpoint := fmt.Sprintf("/space/%s/pages", spaceUUID)
	var content string
	if content, err = c.Get(ProductWiki, endpoint); err != nil {
		return
	}
	err = json.Unmarshal([]byte(gjson.Get(content, "pages").String()), &pages)
	return
}

func (c *Client) DeletePage(spaceUUID, pageUUID string) error {
	if spaceUUID == "" || pageUUID == "" {
		return fmt.Errorf("space_uuid and page_uuid cannot be empty")
	}
	endpoint := fmt.Sprintf("/space/%s/page/%s/delete", spaceUUID, pageUUID)
	_, err := c.Post(ProductWiki, endpoint, nil)
	return err
}
