package ones

import (
	"fmt"
	"testing"
)

func TestListPagesBySpaceUUID(t *testing.T) {
	setup()
	pages, err := testClient.ListPagesBySpaceUUID(SpaceUUIDServiceDepartment)
	if err != nil {
		t.Error(err)
	} else {
		for _, page := range pages {
			fmt.Printf("page_uuid=%s, parent_page_uuid=%s", page.UUID, page.ParentUUID)
		}
	}
}
