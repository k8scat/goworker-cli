package ones

import (
	"fmt"
)

const (
	TaskLinkTypeUUIDWorkOrderTransfer               = "NUfLbfZt" // 客户工单流转 in-转入 out-转出
	TaskLinkTypeUUIDPrivateInstanceDemandDeliver    = "UhrxS2u5" // 私有云需求交付 in-需求 out-实施申请
	TaskLinkTypeUUIDPrivateInstanceWorkOrderDeliver = "Q1KMLH3z" // 私有云工单交付 in-工单 out-实施申请
	TaskLinkTypeUUIDPrivateInstanceManagement       = "UoSpjAPC" // 私有云实例管理 in-实例 out-实施申请
	TaskLinkTypeUUIDPrivateInstanceDeploy           = "Nom5Tqcq" // 私有云部署 in-SaaS版本 out-实施申请
	TaskLinkTypeUUIDSubReleaseApply                 = "VRcL8UXu" // in-父组件 out-子组件
	TaskLinkTypeUUIDRelatedReleaseApply             = "LCRyRDDR" // out-修复版本

	LinkDescTypeOut = "link_out_desc"
	LinkDescTypeIn  = "link_in_desc"

	TransitionUUIDCollectUpgradeInfo         = "N6Cy9LHx" // 收集升级信息: 未开始 -> 升级信息收集
	TransitionUUIDDelivered                  = "WfHrgm37" // 已交付: 已发布 -> 已交付
	TransitionUUIDTransferredDevops          = "zhSXqLSL" // 申请转运维 -> 已转运维组
	TransitionUUIDTransferredProduct         = "RXr8KTYd" // 申请转产品 -> 已转产品部
	TransitionUUIDTransferredDeploy          = "C1UYu9iG" // 申请转交付 -> 已转交付组
	TransitionUUIDTransferredStandardProduct = "P57DCfZV" // 申请转标品 -> 已转标品组
	TransitionUUIDTransferredAvailability    = "CS71JRqC" // 申请转可用性 -> 已转可用性
	TransitionUUIDDeployed                   = "WfHrgm37" // 已发布 -> 已交付
	TransitionUUIDCollectUpgradeInfoDone     = "3Ey3Drbw" // 提交审批: 升级信息收集 -> 待审批
	TransitionUUIDChangeVersion              = "RsDbBjRX" // 更换版本: 任务状态 -> 未开始
	TransitionUUIDTransferred                = "R41GBL3C" // 工单转出: 申请转出 -> 已转出
)

func (c *Client) UpdateTaskStatus(taskUUID, transitionUUID string) error {
	endpoint := fmt.Sprintf("/task/%s/new_transit", taskUUID)
	data := map[string]interface{}{
		"transition_uuid": transitionUUID,
	}
	_, err := c.Post(ProductProject, endpoint, data)
	return err
}

func (c *Client) NewTransit(taskUUID, transitionUUID, discussionText string, fieldValues []map[string]interface{}) error {
	endpoint := fmt.Sprintf("/task/%s/new_transit", taskUUID)
	data := map[string]interface{}{
		"transition_uuid": transitionUUID,
	}
	if discussionText != "" {
		data["discussion"] = map[string]interface{}{
			"text": discussionText,
			"uuid": GenerateUUID(""),
		}
	}
	if fieldValues != nil && len(fieldValues) > 0 {
		data["field_values"] = fieldValues
	}
	_, err := c.Post(ProductProject, endpoint, data)
	return err
}
