package ones

import "testing"

func TestUpdateTaskStatus(t *testing.T) {
	setup()
	taskUUID := "5ZPerY1KLmfF8yWT"
	if err := testClient.UpdateTaskStatus(taskUUID, TransitionUUIDCollectUpgradeInfoDone); err != nil {
		t.Error(err)
	}
}
